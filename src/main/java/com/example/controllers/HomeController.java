package com.example.controllers;

import com.example.model.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@RestController
@RequestMapping("/")
public class HomeController {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private Environment env;

    @RequestMapping("/")
    public String home() {
        return "WELCOME TO SEARCH SERVICE running at port: " + env.getProperty("local.server.port");
    }

    @GetMapping("/category")
    public Category getAllBook() {
        List<Object> books = restTemplate.getForObject("http://book-service/books", List.class);

        Category category = new Category();
        category.setId(1);
        category.setName("หมวดหมู่ทั่วไป");
        category.setBook(books);

        return category;
    }

    @RequestMapping("/admin")
    public String homeAdmin() {
        return "This is the admin area of search service running at port: " + env.getProperty("local.server.port");
    }
}
