package com.example.model;

import java.util.List;

public class Category {
    private Integer id;
    private String name;
    private List<Object> Book;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Object> getBook() {
        return Book;
    }

    public void setBook(List<Object> book) {
        Book = book;
    }
}
